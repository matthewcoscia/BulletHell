using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollDownBehavior : MonoBehaviour
{
    // Used to prevent Ship from following mouse off the screen
    private float speed = 2f;
    private Vector3 lowerLeft;

    // Start is called before the first frame update
    void Start()
    {
        lowerLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, transform.position.z - Camera.main.transform.position.z));
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += transform.up * -1.0f * speed * Time.deltaTime;

        if (this.transform.position.y < lowerLeft.y)
        {
            this.gameObject.SetActive(false);
        }
    }
}
