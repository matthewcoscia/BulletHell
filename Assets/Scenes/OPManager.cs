using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OPManager : MonoBehaviour
{
    // Pool variables
    private List<GameObject> boostPool;
    public GameObject boostObject;
    private List<GameObject> enemyPool;
    public GameObject enemyObject;
    private List<GameObject> sideChickPool;
    public GameObject sideChickObject;

    private float boostActionTime = 2.6f;
    private float enemyActionTime = 2.3f;
    private float sideChickActionTime = 2.0f;
    public int BoostRate = 1;
    public int EnemyRate = 1;
    public int SideShipRate = 1;

    private int amountToPool = 10;
    private int amountSideChicks = 4;

    Vector3 upperLeft;
    Vector3 upperRight;

    // Start is called before the first frame update
    void Start()
    {
        // Set up Pools
        boostPool = new List<GameObject>();
        enemyPool = new List<GameObject>();
        sideChickPool = new List<GameObject>();
        GameObject temp;
        for (int i = 0; i < amountToPool; i++)
        {
            temp = Instantiate(boostObject);
            temp.SetActive(false);
            boostPool.Add(temp);

            temp = Instantiate(enemyObject);
            temp.SetActive(false);
            enemyPool.Add(temp);
        }
        for (int i = 0; i < amountSideChicks; i++)
        {
            temp = Instantiate(sideChickObject);
            temp.SetActive(false);
            sideChickPool.Add(temp);
        }

        // Get top of screen 
        upperRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width,
                                                                Screen.height,
                                                                transform.position.z - Camera.main.transform.position.z));

        upperLeft = Camera.main.ScreenToWorldPoint(new Vector3(0,
                                                               Screen.height, 
                                                               transform.position.z - Camera.main.transform.position.z));
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > sideChickActionTime)
        {
            sideChickActionTime = SideShipRate + Time.time; 
            AddSideChick();
        }

        if (Time.time > enemyActionTime)
        {
            enemyActionTime = EnemyRate + Time.time; 
            AddEnemy();
        }

        if (Time.time > boostActionTime)
        {
            boostActionTime = BoostRate + Time.time; 
            AddBoost();
        }
    }

    private void AddSideChick()
    {
        GameObject sc = GetPoolObject(1);
        if (sc != null)
        {
            sc.transform.position = new Vector3(Random.Range(upperLeft.x, upperRight.x), upperRight.y, 0);
            sc.SetActive(true);
        }
    }

    private void AddEnemy()
    {
        GameObject e = GetPoolObject(3);
        if (e != null)
        {
            e.transform.position = new Vector3(Random.Range(upperLeft.x, upperRight.x), upperRight.y, 0);
            e.SetActive(true);
        }
    }

    private void AddBoost()
    {
        GameObject b = GetPoolObject(2);
        if (b != null)
        {
            b.transform.position = new Vector3(Random.Range(upperLeft.x, upperRight.x), upperRight.y, 0);
            b.SetActive(true);
        }
    }

    private GameObject GetPoolObject(int type)
    {
        List<GameObject> list = null;
        int listSize = 0;
        if (type == 1)
        {
            list = sideChickPool;
            listSize = amountSideChicks;
        }
        else if (type == 2)
        {
            list = boostPool;
            listSize = amountToPool;
        }
        else if (type == 3)
        {
            list = enemyPool;
            listSize = amountToPool;
        }
    
        for (int i = 0; i < listSize; i++)
        {
            if (!list[i].activeInHierarchy)
            {
                return list[i];
            }
        }
        return null;
    }
}
