using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainShipBehavior : MonoBehaviour
{
    private Vector3 mousePos;
    // Used to prevent Ship from following mouse off the screen
    private Bounds cameraBounds;
    private List<GameObject> bulletPool;
    public GameObject bulletObject;
    public int amountToPool;

    private float nextActionTime = 0.0f;
    private float period = 1.0f;

    public int recruits;

    private int[] recruitXOffsets;

    // Start is called before the first frame update
    void Start()
    {
        recruits = 0;
        recruitXOffsets = new int[4];
        
        // Set up Bullet Pool
        bulletPool = new List<GameObject>();
        GameObject temp;
        for (int i = 0; i < amountToPool; i++)
        {
            temp = Instantiate(bulletObject);
            temp.SetActive(false);
            bulletPool.Add(temp);
        }
        
        // Set Camera Bounds
        Vector3 center = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2,
                                                                    Screen.height / 2,
                                                                    transform.position.z - Camera.main.transform.position.z));

        Vector3 upperRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width,
                                                                        Screen.height,
                                                                        transform.position.z - Camera.main.transform.position.z));

        Vector3 lowerLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, transform.position.z - Camera.main.transform.position.z));

        cameraBounds = new Bounds(center, upperRight - lowerLeft);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1));

        if (!cameraBounds.Contains(mousePos))
        {
            Vector3 closestPoint = cameraBounds.ClosestPoint(mousePos);

            this.transform.position = closestPoint;
        }
        else
        {
            this.transform.position = mousePos;
        }

        // Handling Firing
        if (Input.GetMouseButtonDown(0) && Time.time > nextActionTime)
        {
            nextActionTime = period + Time.time; // Enforce fire rate
            fire();
        }
    }

    //This is called on right click
    public void fire()
    {
        activateBullet(this.transform.position);

        //activate enemies
        for (int i = 0; i < recruits; i++)
        {
            activateBullet(new Vector3(this.transform.position.x + recruitXOffsets[i],
                                       this.transform.position.y,
                                       this.transform.position.z));
        }
    }

    void activateBullet(Vector3 pos)
    {
        GameObject bullet = GetPooledObject();
        if (bullet != null)
        {
            bullet.transform.position = pos;
            bullet.SetActive(true);
        }
    }

    GameObject GetPooledObject() 
    {
        for (int i = 0; i < amountToPool; i++)
        {
            if (!bulletPool[i].activeInHierarchy)
            {
                return bulletPool[i];
            }
        }
        return null;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.SetActive(false);
            Debug.Log("Hit ship");
        }
        else if (collision.gameObject.tag == "Boost")
        {
            applyBoost(collision.gameObject);
            Debug.Log("Hit boost");
        }
    }

    public void addSideChick()
    {
        recruitXOffsets[recruits] = positionRecruit();
        recruits++;
    }

    public void applyBoost(GameObject col)
    {
        if (period > 0)
        {
            period -= 0.1f;
        }

        col.SetActive(false);
    }

    public int positionRecruit()
    {
        if (recruits == 0)
        {
            return 1;
        }

        if (recruits == 1)
        {
            return -1;
        }

        if (recruits == 2)
        {
            return 2;
        }

        if (recruits == 3)
        {
            return -2;
        }

        return 0;
    }
}
