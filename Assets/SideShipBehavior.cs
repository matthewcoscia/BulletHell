using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideShipBehavior : MonoBehaviour
{
    bool recruited;
    MainShipBehavior MotherShip;

    private float speed = 2f;
    private Vector3 lowerLeft;

    int MotherShipXOffset;

    // Start is called before the first frame update
    void Start()
    {
        recruited = false;
        
        lowerLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, transform.position.z - Camera.main.transform.position.z));
    }

    // Update is called once per frame
    void Update()
    {
        if (!recruited)
        {
            this.transform.position += transform.up * -1.0f * speed * Time.deltaTime;

            if (this.transform.position.y < lowerLeft.y)
            {
                this.gameObject.SetActive(false);
            }
        }
        else
        {
            this.transform.position = new Vector3(MotherShip.transform.position.x + MotherShipXOffset,
                                                  MotherShip.transform.position.y - 1,
                                                  MotherShip.transform.position.z);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "MainShip")
        {
            joinMotherShip();
            Debug.Log("Joined Main Ship");
        }
        else if (recruited && collision.gameObject.tag == "Boost")
        {
            MotherShip.applyBoost(collision.gameObject);
            Debug.Log("Side Ship Collected boost");
        }
        else if (recruited && collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.SetActive(false);
            Debug.Log("Side Ship Hit Enemy");
        }
        else if (!recruited && collision.gameObject.tag == "SideShip")
        {
            joinMotherShip();
            Debug.Log("Side Ship Recruited another side ship");
        }
    }

    public void joinMotherShip()
    {
        recruited = true;
        MotherShip = GameObject.FindObjectOfType<MainShipBehavior>();
        MotherShipXOffset = MotherShip.positionRecruit();
        MotherShip.addSideChick();
    }
}
