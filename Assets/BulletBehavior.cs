using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour
{
    // Used to prevent Ship from following mouse off the screen
    private Bounds cameraBounds;
    private float speed = 5f;

    // Start is called before the first frame update
    void Start()
    {
        // Set Camera Bounds
        Vector3 center = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2,
                                                                    Screen.height / 2,
                                                                    transform.position.z - Camera.main.transform.position.z));

        Vector3 upperRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width,
                                                                        Screen.height,
                                                                        transform.position.z - Camera.main.transform.position.z));

        Vector3 lowerLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, transform.position.z - Camera.main.transform.position.z));

        cameraBounds = new Bounds(center, upperRight - lowerLeft); 
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += transform.up * speed * Time.deltaTime;

        if (!cameraBounds.Contains(this.transform.position))
        {
            this.gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.SetActive(false);
            this.gameObject.SetActive(false);
            Debug.Log("Hit ship");
        }
    }
}
