# BulletHell

An implementation of Bullet Hell for Unity which uses an object pool
to lower the burden on the CPU from rapidly creating and destroying 
objects. 

## Objects
Main Ship: Fires bullets, sinks enemy ships, recruits side ships
and collects boost.

Side Ship: Once recruited will act alongside the main ship.

Bullets: Spawned by main or side ships and only affect enemy ships.

Boosts: Increase the firing rate of the main ship.

## Files
'Assets/Scenes' contains the C# scripts for the object pool and 
the main pirate ship.

'Assets' contains the C# scripts for bullets, side ships and the 
scrolling window.

'Assets/prefab' contains the visual representations of the main ship,
side ships, enemy ships and bullets.
